# Importing the math module
import math

# Accessing a function from the math module
result = math.sqrt(16)  # sqrt() function to calculate the square root

# Accessing a constant from the math module
pi_value = math.pi  # PI constant

# Printing the results
print("The square root of 16 is:", result)
print("The value of PI is:", pi_value)
