# Custom math utility module

def sqrt(number):
    """Calculate the square root of a number."""
    return number ** 0.5

# Define a constant for PI
PIA = 3
