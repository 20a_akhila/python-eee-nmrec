import math

def celsius_to_fahrenheit(celsius):
    """Convert Celsius to Fahrenheit using math.pow."""
    # Using math.pow for demonstration (celsius * 9/5) + 32
    F = math.pow(celsius, 1) * 9/5 + 32
    return math.floor(F)

def fahrenheit_to_celsius(fahrenheit):
    """Convert Fahrenheit to Celsius using math.pow."""
    # Using math.pow for demonstration ((fahrenheit - 32) * 5/9)
    C = (math.pow(fahrenheit - 32, 1) * 5) / 9
    return math.ceil(C)

def main():
    print("Temperature Conversion Tool")
    print("1. Celsius to Fahrenheit")
    print("2. Fahrenheit to Celsius")

    choice = input("Choose the conversion (1 or 2): ")

    if choice == '1':
        celsius = float(input("Enter temperature in Celsius: "))
        fahrenheit = celsius_to_fahrenheit(celsius)
        print(f"{celsius}°C is equal to {fahrenheit}°F")
    elif choice == '2':
        fahrenheit = float(input("Enter temperature in Fahrenheit: "))
        celsius = fahrenheit_to_celsius(fahrenheit)
        print(f"{fahrenheit}°F is equal to {celsius}°C")
    else:
        print("Invalid choice.")

if __name__ == "__main__":
    main()
