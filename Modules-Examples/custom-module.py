# Importing the custom module
import mathutils

# Using the sqrt function from the custom module
result = mathutils.sqrt(16)
print("The square root of 16 is:", result)

# Accessing the PI constant from the custom module
pi_value = mathutils.PIA
print("The value of PI is:", pi_value)
