# Importing the Calculator class
from calculator import Calculator

# Creating an instance of the Calculator class
calc = Calculator()

# Using methods from the Calculator class
sum_result = calc.add(10, 5)
product_result = calc.multiply(10, 5)
sub_result = calc.sub(10, 5)

# Printing the results
print("Sum of 10 and 5 is:", sum_result)
print("Product of 10 and 5 is:", product_result)
print("Sub of 10 and 5 is:", sub_result)
