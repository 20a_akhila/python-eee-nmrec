# Importing the math module
import math

# Accessing a method from the math module
result = math.ceil(4.2)  # The ceil() method rounds a number up to the nearest integer

# Printing the result
print("The ceiling of 4.2 is:", result)
