# NMREC EEE PYTHON WORKSHOP TASKS

## TASKS ON MODULES

### 1. Temperature Conversion Tool (Using math Module)

#### Exercise Description:

- Create a Python script to convert temperatures between Fahrenheit and Celsius.
- Use the math module to perform any necessary mathematical operations.
- The script should accept user input for the temperature and the desired conversion (Fahrenheit to Celsius or vice versa).

#### Mathematical Formulae:

- Celsius to Fahrenheit: Fahrenheit = 9/5 × Celsius + 32
- Fahrenheit to Celsius: Celsius = 5/9 × (Fahrenheit − 32)

#### Modules to be Used:

- math: For any additional mathematical operations required.


### 2. Simple Signal Analyzer (Using numpy and matplotlib Modules)

#### Exercise Description:

- Develop a Python script that generates and analyzes a simple sine wave signal.
- Use numpy to create a sine wave array and perform any calculations.
- Plot the sine wave using matplotlib to visualize it.
- The script should allow the user to specify the frequency and amplitude of the sine wave.

#### Mathematical Formulae:

- Sine Wave: A×sin(2πft)
    - Where A is the amplitude, f is the frequency, and t is the time array.

#### Modules to be Used:

- numpy: For creating sine wave arrays and handling calculations.
- matplotlib: For plotting the sine wave.


### 3. Electrical Circuit Solver (Using scipy and numpy Modules)

#### Exercise Description:

- Create a Python script that solves a simple electrical circuit using Kirchhoff’s laws.
- The circuit can consist of resistors, a voltage source, and a current source.
- Use numpy for matrix operations and scipy to solve the system of equations.
- Allow the user to input values for resistances, voltage, and current.

#### Mathematical Concept:

##### Kirchhoff’s Laws:

###### - Kirchhoff’s Current Law (KCL): 
- States that the total current entering a junction (or node) in a circuit must equal the total current leaving the junction. Mathematically, for a junction 
∑Iin = ∑Iout
- This law is based on the principle of conservation of charge.

###### - Kirchhoff’s Voltage Law (KVL):

- States that the sum of all the electrical potential differences (voltages) around any closed loop in a circuit must equal zero. For a loop 
∑V=0
- This law is based on the principle of conservation of energy.

##### Solving a Circuit:

To solve a circuit using these laws, you typically follow these steps:

- Identify Nodes and Loops:
    - Label each junction or node in the circuit.
    - Identify distinct loops for applying KVL.
- Set Up Equations:
    - Apply KCL at each node (except the ground node) to set up current equations.
    - Apply KVL in each loop to set up voltage equations.
- Matrix Representation:
    - Express the set of linear equations in matrix form AX=B, where:
        - A is the coefficient matrix containing the resistances and signs based on the circuit's layout.
        - X is the column matrix of unknowns (currents or voltages).
        - B is the column matrix of constants (supply voltages or currents).
- Solve the Matrix Equation: Use linear algebra techniques to solve the matrix equation. In Python, you can use numpy for matrix operations and scipy.linalg for solving the system.

#### Modules to be Used:

- numpy: For creating matrices and handling linear algebra operations.
- scipy.linalg: For solving the system of linear equations.


## TASKS ON METHODS

### 1. Ohm's Law Calculator

#### Exercise Description:

- Create a Python class named OhmsLaw with methods to calculate voltage, current, and resistance using Ohm's Law.
- The class should have three methods: calculate_voltage, calculate_current, and calculate_resistance.
- Each method should take the required parameters and return the calculated value.

#### Mathematical Formulae:

- Ohm's Law: V=I×R
    - Where V is voltage, I is current, and R is resistance.

#### Modules to be Used:

- No external modules are required for this exercise.


### 2. AC Power Calculator

#### Exercise Description:

- Develop a Python class named ACPowerCalculator.
- The class should include methods to calculate real power, reactive power, apparent power, and power factor in an AC circuit.
- Inputs should include voltage, current, and phase angle.

#### Mathematical Formulae:

- Real Power (P): P=V×I×cos(ϕ)
- Reactive Power (Q): Q=V×I×sin(ϕ)
- Apparent Power (S): S=V×I
- Power Factor: cos(ϕ)
- Where V is RMS voltage, I is RMS current, and ϕ is the phase angle.

#### Modules to be Used:

- math: For trigonometric functions.


### 3. RLC Circuit Resonant Frequency Calculator

#### Exercise Description:

- Create a Python class named RLCCircuit.
- The class should include methods to calculate the resonant frequency of a series RLC circuit and a parallel RLC circuit.
- The methods should accept values for resistance (R), inductance (L), and capacitance (C).

#### Mathematical Formulae:

- [Click here for forumulae](https://gitlab.com/20a_akhila/python-eee-nmrec/-/blob/910f18942254212d5c8898721c6971f6947a8597/Screenshot_from_2023-12-19_15-31-49.png)
    - L is inductance in henries, C is capacitance in farads, R is resistance in ohms.

#### Modules to be Used:

- math: For square root and PI constant.
